import sys
import numpy as np

def frame(file1):
    raw = np.fromfile(file1, dtype=np.ubyte)
    zeros_front = np.zeros((1010,), dtype=np.ubyte)
    zeros_end = np.zeros((1010,), dtype=np.ubyte)
    slice_data = np.array([], dtype=np.ubyte)
    pattern = np.array([], dtype=np.ubyte)
    final = np.array([], dtype=np.ubyte)

    '''Takes the size of the array and converts into a 32bit binary representation.
    Then, splits the 32bit into 4 bytes. Next, turns the bytes into its decimal/ubyte
    representation. Converts the list into a numpy array. Finally, appends the sliced
    data to the numpy array representing the length'''
    len_raw = format(raw.size, '064b')
    bin_len_raw = [len_raw[i:i+8] for i in range(0, len(len_raw), 8)]
    temp_raw_arr = []
    for i in bin_len_raw:
        temp_raw_arr.append(int(i,2))

    arr_raw_len = np.array(temp_raw_arr, dtype=np.ubyte)

    '''While loop: adds a 0-255 number every 1023 bytes'''
    i = 0
    while raw.size > 0:
        val = np.array([0], dtype=np.ubyte)
        val[0] = np.add(val[0], i)
        i += 1
        y = raw[0:1023]
        raw = raw[1023:]
        y = np.append(y, val)
        slice_data = np.append(slice_data, y)

        if val[0] == 255:
            i = 0
        
        if raw.size < 1023:
            left_over = 1023 - raw.size
            zero = np.zeros((left_over,), dtype=np.ubyte)
            slice_data = np.append(slice_data, raw)
            slice_data = np.append(slice_data, zero)
            val = np.array([0], dtype=np.ubyte)
            val[0] = np.add(val[0], i)
            slice_data = np.append(slice_data, val)
            raw = np.array([], dtype=np.ubyte)

    '''Takes the size of the array and converts into a 32bit binary representation.
    Then, splits the 32bit into 4 bytes. Next, turns the bytes into its decimal/ubyte
    representation. Converts the list into a numpy array. Finally, appends the sliced
    data to the numpy array representing the length'''
    len_sd = format(slice_data.size, '064b')
    bin_len_sd = [len_sd[i:i+8] for i in range(0, len(len_sd), 8)]
    temp_arr = []
    for i in bin_len_sd:
        temp_arr.append(int(i,2))

    arr_sd_len = np.array(temp_arr, dtype=np.ubyte)
    pattern = np.append(arr_sd_len, slice_data)
    pattern = np.append(pattern, arr_raw_len)

    '''Converts the word raptor into its decimal/ubyte form. Next, converts the list
    to a numpy array. The previous pattern array appends onto the raptor np array.
    Finally, the raptor np array appends onto the pattern array. The pattern is 
    complete at this step.'''
    sen = 'raptor'
    rap = []
    for char in sen:
        rap.append(ord(char))
    
    np_rap = np.array(rap, dtype=np.ubyte)

    pattern = np.append(np_rap, pattern)
    pattern = np.append(pattern, np_rap)

    '''Finally, the pattern appends to the array of 1014 zeros, and that new array has
    the 1018 zeros append onto it.'''
    final = np.append(zeros_front, pattern)
    final = np.append(final, zeros_end)
    return final


def main():
    file_name = sys.argv[1]
    output = frame(file_name)
    output.astype(dtype=np.ubyte)
    output.tofile(sys.argv[2])
    
if __name__ == '__main__':
    main()