#!/usr/bin/env python3

import numpy as np

def unpack(in_fname, out_fname):
   '''Unpackes the data (turns the bytes into bits
    
    Keyword Arguments:
    in_fname -- File name that is inputted
    out_fname -- Output file that was unpacked'''
   packed_data = np.fromfile(in_fname, dtype=np.uint8)
   unpacked_bits = np.unpackbits(packed_data)
   unpacked_bits.tofile(out_fname)


if __name__ == "__main__":
    import sys
    unpack(sys.argv[1], sys.argv[2])
