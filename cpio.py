import os
import sys
import subprocess

def main():
    directory = sys.argv[1]
    file_path = sys.argv[2]
    cpio_path_file = sys.argv[3]
    os.chdir(directory)
    
    subprocess.run(['ls ' + file_path +' | cpio -o > ' + cpio_path_file], shell = True)

    
if __name__ == '__main__':
    main()