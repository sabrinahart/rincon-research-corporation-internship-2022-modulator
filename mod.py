import numpy as np

def unpack(in_fname, out_fname):
    '''Unpackes the data (turns the bytes into bits
    
    Keyword Arguments:
    in_fname -- File name that is inputted
    out_fname -- Output file that was unpacked'''
    packed_data = np.fromfile(in_fname, dtype=np.uint8)
    unpacked_bits = np.unpackbits(packed_data)
    unpacked_bits.tofile(out_fname)

def bpsk(in_fname, out_fname):
    '''Turned the bits into phases. The phases array is returned.
    
    Keyword Arguments:
    in_fname -- Input file that has the bits
    out_fname -- Output file that represents the phases'''
    symbols = np.array([1,-1], dtype=np.csingle)
    lut = {1:symbols[1], 0:symbols[0]}
    in_data = np.fromfile(in_fname, dtype=np.uint8)
    out_data = np.array([lut[x] for x in in_data], dtype=np.csingle)
    out_arr = out_data
    out_data.tofile(out_fname)
    return out_arr

def scaling(in_arr, out_fname):
    '''Scales the data into int16 so they Raptor can receives the data.
    
    Keyword Arguments:
    in_arr -- Numpy Array of the phases
    out_fname -- Output file that represents the scaled phases'''
    bit_size = 15
    new_array = np.zeros((2*in_arr.size,), dtype=np.int16)
    new_array[0::2] = np.round(in_arr.real * (2**bit_size - 1))
    new_array[1::2] = np.round(in_arr.imag * (2**bit_size - 1))
    new_array.tofile(out_fname)



if __name__ == "__main__":
    import sys
    unpack(sys.argv[1], sys.argv[2])
    bpsk(sys.argv[2], sys.argv[3])
    arr = bpsk(sys.argv[2], sys.argv[3])
    scaling(arr, sys.argv[4])