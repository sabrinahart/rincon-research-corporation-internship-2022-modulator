# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app1_gui.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_app1_gui(object):
    def setupUi(self, app1_gui):
        app1_gui.setObjectName("app1_gui")
        app1_gui.resize(1216, 753)
        app1_gui.setStyleSheet("background-color: rgb(186, 189, 182);\n"
"background-color: rgb(211, 215, 207);")
        self.Imagelabel = QtWidgets.QLabel(app1_gui)
        self.Imagelabel.setGeometry(QtCore.QRect(30, 30, 375, 331))
        self.Imagelabel.setText("")
        self.Imagelabel.setObjectName("Imagelabel")
        self.frdata_frame = XmFrame(app1_gui)
        self.frdata_frame.setGeometry(QtCore.QRect(430, 310, 281, 431))
        self.frdata_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frdata_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frdata_frame.setObjectName("frdata_frame")
        self.file_exp = QtWidgets.QFrame(app1_gui)
        self.file_exp.setGeometry(QtCore.QRect(30, 380, 381, 361))
        self.file_exp.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.file_exp.setFrameShadow(QtWidgets.QFrame.Raised)
        self.file_exp.setObjectName("file_exp")
        self.gridLayout = QtWidgets.QGridLayout(self.file_exp)
        self.gridLayout.setObjectName("gridLayout")
        self.treeView = QtWidgets.QTreeView(self.file_exp)
        self.treeView.setObjectName("treeView")
        self.gridLayout.addWidget(self.treeView, 0, 0, 1, 1)
        self.IQ_frame = XmFrame(app1_gui)
        self.IQ_frame.setGeometry(QtCore.QRect(720, 520, 481, 181))
        self.IQ_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.IQ_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.IQ_frame.setObjectName("IQ_frame")
        self.unpacked_frame = XmFrame(app1_gui)
        self.unpacked_frame.setGeometry(QtCore.QRect(430, 50, 771, 191))
        self.unpacked_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.unpacked_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.unpacked_frame.setObjectName("unpacked_frame")
        self.phases_frame = XmFrame(app1_gui)
        self.phases_frame.setGeometry(QtCore.QRect(720, 310, 481, 181))
        self.phases_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.phases_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.phases_frame.setObjectName("phases_frame")
        self.frame_button = QtWidgets.QPushButton(app1_gui)
        self.frame_button.setGeometry(QtCore.QRect(430, 250, 281, 36))
        self.frame_button.setStyleSheet("background-color: rgb(238, 238, 236);")
        self.frame_button.setObjectName("frame_button")
        self.fire_button = QtWidgets.QPushButton(app1_gui)
        self.fire_button.setGeometry(QtCore.QRect(830, 710, 281, 36))
        self.fire_button.setStyleSheet("background-color: rgb(238, 238, 236);")
        self.fire_button.setObjectName("fire_button")
        self.unpack_label = QtWidgets.QLabel(app1_gui)
        self.unpack_label.setGeometry(QtCore.QRect(720, 30, 261, 20))
        self.unpack_label.setObjectName("unpack_label")
        self.phas_data = QtWidgets.QLabel(app1_gui)
        self.phas_data.setGeometry(QtCore.QRect(720, 290, 251, 20))
        self.phas_data.setObjectName("phas_data")
        self.IQ_label = QtWidgets.QLabel(app1_gui)
        self.IQ_label.setGeometry(QtCore.QRect(720, 500, 341, 20))
        self.IQ_label.setObjectName("IQ_label")
        self.fr_raster_label = QtWidgets.QLabel(app1_gui)
        self.fr_raster_label.setGeometry(QtCore.QRect(430, 290, 281, 20))
        self.fr_raster_label.setObjectName("fr_raster_label")
        self.mod_button = QtWidgets.QPushButton(app1_gui)
        self.mod_button.setGeometry(QtCore.QRect(830, 250, 281, 36))
        self.mod_button.setStyleSheet("background-color: rgb(238, 238, 236);")
        self.mod_button.setObjectName("mod_button")
        self.unpack_button = QtWidgets.QPushButton(app1_gui)
        self.unpack_button.setGeometry(QtCore.QRect(430, 10, 281, 36))
        self.unpack_button.setStyleSheet("background-color: rgb(238, 238, 236);")
        self.unpack_button.setObjectName("unpack_button")

        self.retranslateUi(app1_gui)
        QtCore.QMetaObject.connectSlotsByName(app1_gui)

    def retranslateUi(self, app1_gui):
        _translate = QtCore.QCoreApplication.translate
        app1_gui.setWindowTitle(_translate("app1_gui", "Form"))
        self.frame_button.setText(_translate("app1_gui", "FRAME DATA"))
        self.fire_button.setText(_translate("app1_gui", "FIRE TO RAPTOR"))
        self.unpack_label.setText(_translate("app1_gui", "First Frame on Data: Unpacked Plot"))
        self.phas_data.setText(_translate("app1_gui", "Third Frame on Data: Phase Plot"))
        self.IQ_label.setText(_translate("app1_gui", "Third Frame on Data: IQ Plot"))
        self.fr_raster_label.setText(_translate("app1_gui", "Second Frame on Data: Raster Plot"))
        self.mod_button.setText(_translate("app1_gui", "MODULATE DATA"))
        self.unpack_button.setText(_translate("app1_gui", "UNPACK DATA"))


from xmframe import XmFrame
