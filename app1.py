from email.charset import QP
import os
import subprocess
import sys

import PyQt5.QtCore
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QFileSystemModel, QApplication, QWidget
from xminter.xmidas import xm
from xmframe import QtXPrimitive

from app1_gui import Ui_app1_gui


class App1(QWidget, Ui_app1_gui):
    def __init__(self, path_file, parent=None):
        super(App1, self).__init__(parent)
        self.setup_ui()
        self.dir_path = path_file
        self.pic_path = ''
        self.cpio_path = ''
        self.double_cpio_path = ''
        self.frame_path = ''
        self.scaled_frame = ''
        self.basename = ''
        self.name = ''
        self.images_path = '/home/smh/images/'
        self.scratch_path = '/scratch/smh/'

        self.unpack_button.clicked.connect(self.clicked_unpack_button)
        self.frame_button.clicked.connect(self.clicked_frame_button)
        self.mod_button.clicked.connect(self.clicked_mod_button)
        self.fire_button.clicked.connect(self.clicked_fire_button)
        self.file_explorer()

    def setup_ui(self):
        self.setupUi(self)

    def file_explorer(self):
        self.model = QFileSystemModel()
        self.model.setRootPath(PyQt5.QtCore.QDir.rootPath())
        self.treeView.setModel(self.model)
        self.treeView.setRootIndex(self.model.index(self.images_path))
        self.treeView.setSortingEnabled(True)
        self.treeView.clicked.connect(self.get_file)
 
    def get_file(self, signal):
        self.pic_path = self.model.filePath(signal)
        self.display_file(self.pic_path)

    def display_file(self, fname):
        pixmap = QPixmap(fname)
        pixmap = pixmap.scaled(375, 331, PyQt5.QtCore.Qt.KeepAspectRatio)
        self.Imagelabel.setPixmap(pixmap)

    def clicked_unpack_button(self):
        #Paths
        self.basename = os.path.basename(self.pic_path)
        self.name = os.path.splitext(self.basename)[0]

        unp_orig = self.dir_path + 'unp_cpio_' + self.name + '.unp '
        self.cpio_path = self.dir_path + self.name + '.cpio '

        #Python Script
        os.chdir('/home/smh/raptorbooth/smh/')
        subprocess.run(['python3 cpio.py ' + self.images_path + ' ' + self.basename + ' ' + self.cpio_path], shell=True)
        subprocess.run(['python3 unpack.py ' + self.cpio_path + unp_orig], shell=True)

        #X Midas 
        os.system('cp ' + unp_orig + self.scratch_path)
        os.chdir(self.scratch_path)
        
        unp_orig_name = 'unp_or_' + self.name[:3]
        xm.dataimport('unp_cpio_' + self.name + '.unp', unp_orig_name, 'SB')

        # Plotting the Xplot plots
        QtXPrimitive(self.unpacked_frame, xm.xplot, unp_orig_name + '.tmp', 
                                                    None, None, 'ph', None, None, -0.125, 1.125,
                                                    trace='(LINE=Connecting, SYM=Circles)',
                                                    NOPAN=True, NOREADOUT=True)

    def clicked_frame_button(self):
        #Paths
        self.frame_path = self.dir_path + self.name + '.frame '
        self.double_cpio_path = self.dir_path + 'two_cpio_' + self.name + '.cpio '
        os.chdir('/home/smh/raptorbooth/smh/')

        #Python Scripts
        subprocess.run(['python3 frame.py ' + self.cpio_path + self.frame_path], shell = True)
        subprocess.run(['python3 cpio.py ' + self.dir_path + ' ' + self.name + '.frame ' + self.double_cpio_path], shell=True)

        #Create X midas frame file
        os.system('cp ' + self.frame_path + self.scratch_path)
        os.chdir(self.scratch_path)
        mid_name = 'fr' + self.name[:3] + '2'
        xm.dataimport(self.name + '.frame', mid_name)

        #Raster Plot
        QtXPrimitive(self.frdata_frame, xm.xraster, mid_name + '(fc=sp)', None, None, None, 8192, None,
                                                    autorr=True, SPECS=0)


    def clicked_mod_button(self):
        os.chdir('/home/smh/raptorbooth/smh/')

        #Paths
        unp_frame = self.dir_path + 'fram_' + self.name + '.unp '
        bpsk_frame = self.dir_path + 'fram_' + self.name + '.bpsk '
        self.scaled_frame = self.dir_path + 'fram_' + self.name + '.scale '

        #Python Script
        subprocess.run(['python3 mod.py ' + self.double_cpio_path + unp_frame + bpsk_frame + self.scaled_frame], shell=True)

        #Create X Midas Files
        os.system('cp -f ' + unp_frame + bpsk_frame + self.scaled_frame + self.scratch_path)
        os.chdir(self.scratch_path)
        unp_fr_name = 'mod_unp_fr_' + self.name[:3]
        pha_fr_name = 'mod_pha_fr_' + self.name[:3]

        xm.dataimport('fram_' + self.name + '.unp', unp_fr_name, 'SB')
        xm.dataimport('fram_' + self.name + '.bpsk', pha_fr_name, 'CF')

        # Plotting the Xplot plots
        QtXPrimitive(self.phases_frame, xm.xplot, pha_fr_name + '.tmp', 
                                                    None, None, 'ph', None, None, -20, 200,
                                                    trace='(LINE=Connecting, SYM=Circles)',
                                                    NOPAN=True, NOREADOUT=True)

        # Plotting the IQ Plot
        QtXPrimitive(self.IQ_frame, xm.xplot, pha_fr_name + '.tmp', 
                                                    -1.25, 1.25, 'ir',
                                                    trace='(LINE=None, SYM=Circles, RAD=10)',
                                                    NOGRID=True, NOPAN=True, NOREADOUT=True)

    def clicked_fire_button(self):
        #Sends to Raptor
        rap_name = 'raptor_' + self.name[:3]
        scaled_name = 'fram_' + self.name + '.scale '

        os.chdir(self.scratch_path)
        xm.dataimport(scaled_name, rap_name, 'CI')
        xm.headermod(rap_name, None, None, None, None, 1/100e3)

        os.system('cp -f ' + rap_name + '.det ' + rap_name + '.tmp /sharepoint/')
        os.chdir('/home/warehouse/')
        
        #Runs the script to send to the Raptor
        subprocess.run(['./tx.sh ' + rap_name + '.det'], shell=True)

     
    def closeEvent(self, e):
        super(App1, self).closeEvent(e)

def main():
    app = QApplication(sys.argv)
    dir_path = '/home/smh/'
    wid = App1(dir_path)
    wid.show()
    app.processEvents()
    app.exec_()


if __name__ == "__main__":
    main()    
